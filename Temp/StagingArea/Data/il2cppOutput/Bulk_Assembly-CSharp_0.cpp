﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// GMPipe
struct GMPipe_t5F62CF8B65F621A973C7EBD778F796C789263A00;
// GameState
struct GameState_t26C77D92567E12173718C059731CF9EB15949FF1;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0;
// UnityEngine.Collider2D
struct Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// clickButton
struct clickButton_tB6EF39F3026E58B94382E57B99B7AC67D7664CF1;
// collideManagementBird
struct collideManagementBird_t10610FE13108B02F6F70FEDA178597557A8005E7;
// endAction
struct endAction_tCB18F82EC3F91864854F9923A00BBFBD8D21043E;
// gameOver
struct gameOver_t8238CCAC5153CC05D10670839980675A5543A6BA;
// moveBK
struct moveBK_tDF80A69114F7791D9C9C69A68D4C9A1C2A428D62;
// movePiece
struct movePiece_t02F2BBBFC2FFB3AE900669CBEB15EBB905F07ED1;
// movePipe
struct movePipe_t6D0094C8A4861DD68281F5DB69D1847251347FD7;
// touchAction
struct touchAction_t3D74B780A5F29E015096A42B6C42736DC2274AA8;
// wait2Sec
struct wait2Sec_t0252EEEDAAD43734C6526C673080AAA07B3759BB;

extern RuntimeClass* Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
extern RuntimeClass* Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral0803DF4FF1650933D2FFE6BE04D4B21432134252;
extern String_t* _stringLiteral2B020927D3C6EB407223A1BAA3D6CE3597A3F88D;
extern String_t* _stringLiteral4DB596DEEE2F98CE13899AEF2B65181A0324BA50;
extern String_t* _stringLiteral71A7CA855F38BC559D0FCE6727C7E2D8ADA99FF8;
extern String_t* _stringLiteral72909F5BC427175A4FF19C04C7D48487D1027642;
extern String_t* _stringLiteral79CBC5EB672A54C2B82572AFD52F2A6A4F85E598;
extern String_t* _stringLiteralA94A8FE5CCB19BA61C4C0873D391E987982FBBD3;
extern String_t* _stringLiteralAAA83EAC6890A9A6E2273EA51D6F2F2915B1A019;
extern String_t* _stringLiteralCD8E5112CDE0E83821B265528F8F201DC85AB434;
extern String_t* _stringLiteralCD92815BF6273ACBAF834B9FAED277C722068291;
extern String_t* _stringLiteralE17AEFFD7383EFB7F735A5AAF3599DFEFAD04B3A;
extern String_t* _stringLiteralF2F001CA869A6E88BBFDF32404B845C6643460EC;
extern String_t* _stringLiteralF86BEE40FEF30A61FB3307AB4DCB825C732FC4C1;
extern const RuntimeMethod* Component_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_m7FAA3F910786B0B5F3E0CBA755F38E0453EAF7BA_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisRigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE_mBF198078E908267FB6DA59F6242FC8F36FC06625_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisRigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE_mDDB82F02C3053DCC0D60C420752A11EC11CBACC0_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mBAC50AEEACC94DE12B47C6D6A5227051B1C6DE12_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisendAction_tCB18F82EC3F91864854F9923A00BBFBD8D21043E_m1CCB1B69336D0BC356BE5509FEEB39EE2614B834_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TistouchAction_t3D74B780A5F29E015096A42B6C42736DC2274AA8_m68E8DCDEF7883D8FEC9C0BDCE629A79B38181134_RuntimeMethod_var;
extern const uint32_t GMPipe_Update_mC7A2C643D177BE7ED633291A807CB3E540E51FCB_MetadataUsageId;
extern const uint32_t clickButton_onClick_m27D31BDD26C0F6C7DA2171A35E9F9531FCE82855_MetadataUsageId;
extern const uint32_t collideManagementBird_OnTriggerEnter2D_m464B528284B9F16FF62ABFB611747C58BCE509CE_MetadataUsageId;
extern const uint32_t collideManagementBird_Start_m4B81301BAEE5A3301C7FB36D88BD380E27D962BF_MetadataUsageId;
extern const uint32_t endAction_Start_m73FAEF2E2CCCF77D662A0D562782A65B97CEEFEA_MetadataUsageId;
extern const uint32_t endAction_stop_m37F7513F1A79900015EFDDC1E211A35B81F7A55B_MetadataUsageId;
extern const uint32_t gameOver_Update_mF4564D995D17F5286D8AD892E0F95006DD04082F_MetadataUsageId;
extern const uint32_t moveBK_Update_m91ADF12C67BC641359F2D3D06E44030B428E3D47_MetadataUsageId;
extern const uint32_t movePipe_Start_mAD310D623CE6B677CB968007AE9353FD3F1270B2_MetadataUsageId;
extern const uint32_t movePipe_Update_m0F53C305A29B3FBB513599A4B9515F8EB0DD26E8_MetadataUsageId;
extern const uint32_t touchAction_Start_mB32FE2F2862E851F7C2BAC94E0E4A9E34BE089F9_MetadataUsageId;
extern const uint32_t touchAction_Update_m7E0E61041918DD12F3B38A43CC7EC78969985904_MetadataUsageId;
extern const uint32_t wait2Sec_Update_m5762D8B269B2A5032B344805C26FAED4289374C6_MetadataUsageId;
extern const uint32_t wait2Sec_goScene2_m73CE9D452FC2392DAA372F155CA55AD055183EC4_MetadataUsageId;

struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520;


#ifndef U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#define U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#define BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Center_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Extents_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#define GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef RENDERER_T0556D67DD582620D1F495627EDE30D03284151F4_H
#define RENDERER_T0556D67DD582620D1F495627EDE30D03284151F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t0556D67DD582620D1F495627EDE30D03284151F4  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T0556D67DD582620D1F495627EDE30D03284151F4_H
#ifndef RIGIDBODY2D_TBDC6900A76D3C47E291446FF008D02B817C81CDE_H
#define RIGIDBODY2D_TBDC6900A76D3C47E291446FF008D02B817C81CDE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rigidbody2D
struct  Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODY2D_TBDC6900A76D3C47E291446FF008D02B817C81CDE_H
#ifndef TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#define TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#ifndef ANIMATOR_TF1A88E66B3B731DDA75A066DBAE9C55837660F5A_H
#define ANIMATOR_TF1A88E66B3B731DDA75A066DBAE9C55837660F5A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animator
struct  Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATOR_TF1A88E66B3B731DDA75A066DBAE9C55837660F5A_H
#ifndef CAMERA_T48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_H
#define CAMERA_T48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields
{
public:
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPreCull_4;
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPreRender_5;
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_4), value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_5), value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_H
#ifndef COLLIDER2D_TD64BE58E48B95D89D349FEAB54D0FE2EEBF83379_H
#define COLLIDER2D_TD64BE58E48B95D89D349FEAB54D0FE2EEBF83379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider2D
struct  Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER2D_TD64BE58E48B95D89D349FEAB54D0FE2EEBF83379_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef SPRITERENDERER_TCD51E875611195DBB91123B68434881D3441BC6F_H
#define SPRITERENDERER_TCD51E875611195DBB91123B68434881D3441BC6F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpriteRenderer
struct  SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F  : public Renderer_t0556D67DD582620D1F495627EDE30D03284151F4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITERENDERER_TCD51E875611195DBB91123B68434881D3441BC6F_H
#ifndef GMPIPE_T5F62CF8B65F621A973C7EBD778F796C789263A00_H
#define GMPIPE_T5F62CF8B65F621A973C7EBD778F796C789263A00_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GMPipe
struct  GMPipe_t5F62CF8B65F621A973C7EBD778F796C789263A00  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject[] GMPipe::respawns
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___respawns_4;
	// UnityEngine.GameObject GMPipe::parentPipe
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___parentPipe_5;

public:
	inline static int32_t get_offset_of_respawns_4() { return static_cast<int32_t>(offsetof(GMPipe_t5F62CF8B65F621A973C7EBD778F796C789263A00, ___respawns_4)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_respawns_4() const { return ___respawns_4; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_respawns_4() { return &___respawns_4; }
	inline void set_respawns_4(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___respawns_4 = value;
		Il2CppCodeGenWriteBarrier((&___respawns_4), value);
	}

	inline static int32_t get_offset_of_parentPipe_5() { return static_cast<int32_t>(offsetof(GMPipe_t5F62CF8B65F621A973C7EBD778F796C789263A00, ___parentPipe_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_parentPipe_5() const { return ___parentPipe_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_parentPipe_5() { return &___parentPipe_5; }
	inline void set_parentPipe_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___parentPipe_5 = value;
		Il2CppCodeGenWriteBarrier((&___parentPipe_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GMPIPE_T5F62CF8B65F621A973C7EBD778F796C789263A00_H
#ifndef GAMESTATE_T26C77D92567E12173718C059731CF9EB15949FF1_H
#define GAMESTATE_T26C77D92567E12173718C059731CF9EB15949FF1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameState
struct  GameState_t26C77D92567E12173718C059731CF9EB15949FF1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 GameState::scorePlayer
	int32_t ___scorePlayer_5;
	// UnityEngine.UI.Text GameState::scoreText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___scoreText_6;

public:
	inline static int32_t get_offset_of_scorePlayer_5() { return static_cast<int32_t>(offsetof(GameState_t26C77D92567E12173718C059731CF9EB15949FF1, ___scorePlayer_5)); }
	inline int32_t get_scorePlayer_5() const { return ___scorePlayer_5; }
	inline int32_t* get_address_of_scorePlayer_5() { return &___scorePlayer_5; }
	inline void set_scorePlayer_5(int32_t value)
	{
		___scorePlayer_5 = value;
	}

	inline static int32_t get_offset_of_scoreText_6() { return static_cast<int32_t>(offsetof(GameState_t26C77D92567E12173718C059731CF9EB15949FF1, ___scoreText_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_scoreText_6() const { return ___scoreText_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_scoreText_6() { return &___scoreText_6; }
	inline void set_scoreText_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___scoreText_6 = value;
		Il2CppCodeGenWriteBarrier((&___scoreText_6), value);
	}
};

struct GameState_t26C77D92567E12173718C059731CF9EB15949FF1_StaticFields
{
public:
	// GameState GameState::Instance
	GameState_t26C77D92567E12173718C059731CF9EB15949FF1 * ___Instance_4;

public:
	inline static int32_t get_offset_of_Instance_4() { return static_cast<int32_t>(offsetof(GameState_t26C77D92567E12173718C059731CF9EB15949FF1_StaticFields, ___Instance_4)); }
	inline GameState_t26C77D92567E12173718C059731CF9EB15949FF1 * get_Instance_4() const { return ___Instance_4; }
	inline GameState_t26C77D92567E12173718C059731CF9EB15949FF1 ** get_address_of_Instance_4() { return &___Instance_4; }
	inline void set_Instance_4(GameState_t26C77D92567E12173718C059731CF9EB15949FF1 * value)
	{
		___Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESTATE_T26C77D92567E12173718C059731CF9EB15949FF1_H
#ifndef CLICKBUTTON_TB6EF39F3026E58B94382E57B99B7AC67D7664CF1_H
#define CLICKBUTTON_TB6EF39F3026E58B94382E57B99B7AC67D7664CF1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// clickButton
struct  clickButton_tB6EF39F3026E58B94382E57B99B7AC67D7664CF1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLICKBUTTON_TB6EF39F3026E58B94382E57B99B7AC67D7664CF1_H
#ifndef COLLIDEMANAGEMENTBIRD_T10610FE13108B02F6F70FEDA178597557A8005E7_H
#define COLLIDEMANAGEMENTBIRD_T10610FE13108B02F6F70FEDA178597557A8005E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// collideManagementBird
struct  collideManagementBird_t10610FE13108B02F6F70FEDA178597557A8005E7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject collideManagementBird::bird
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___bird_4;
	// UnityEngine.GameObject collideManagementBird::GameState
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___GameState_5;
	// UnityEngine.Vector3 collideManagementBird::rightTopCam
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightTopCam_6;
	// UnityEngine.Vector3 collideManagementBird::rightBotCam
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightBotCam_7;
	// UnityEngine.Vector3 collideManagementBird::leftTopCam
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftTopCam_8;
	// UnityEngine.Vector3 collideManagementBird::leftBotCam
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftBotCam_9;
	// System.Int32 collideManagementBird::points
	int32_t ___points_10;
	// System.Int32 collideManagementBird::scorePlayer
	int32_t ___scorePlayer_11;

public:
	inline static int32_t get_offset_of_bird_4() { return static_cast<int32_t>(offsetof(collideManagementBird_t10610FE13108B02F6F70FEDA178597557A8005E7, ___bird_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_bird_4() const { return ___bird_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_bird_4() { return &___bird_4; }
	inline void set_bird_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___bird_4 = value;
		Il2CppCodeGenWriteBarrier((&___bird_4), value);
	}

	inline static int32_t get_offset_of_GameState_5() { return static_cast<int32_t>(offsetof(collideManagementBird_t10610FE13108B02F6F70FEDA178597557A8005E7, ___GameState_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_GameState_5() const { return ___GameState_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_GameState_5() { return &___GameState_5; }
	inline void set_GameState_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___GameState_5 = value;
		Il2CppCodeGenWriteBarrier((&___GameState_5), value);
	}

	inline static int32_t get_offset_of_rightTopCam_6() { return static_cast<int32_t>(offsetof(collideManagementBird_t10610FE13108B02F6F70FEDA178597557A8005E7, ___rightTopCam_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightTopCam_6() const { return ___rightTopCam_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightTopCam_6() { return &___rightTopCam_6; }
	inline void set_rightTopCam_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightTopCam_6 = value;
	}

	inline static int32_t get_offset_of_rightBotCam_7() { return static_cast<int32_t>(offsetof(collideManagementBird_t10610FE13108B02F6F70FEDA178597557A8005E7, ___rightBotCam_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightBotCam_7() const { return ___rightBotCam_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightBotCam_7() { return &___rightBotCam_7; }
	inline void set_rightBotCam_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightBotCam_7 = value;
	}

	inline static int32_t get_offset_of_leftTopCam_8() { return static_cast<int32_t>(offsetof(collideManagementBird_t10610FE13108B02F6F70FEDA178597557A8005E7, ___leftTopCam_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftTopCam_8() const { return ___leftTopCam_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftTopCam_8() { return &___leftTopCam_8; }
	inline void set_leftTopCam_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftTopCam_8 = value;
	}

	inline static int32_t get_offset_of_leftBotCam_9() { return static_cast<int32_t>(offsetof(collideManagementBird_t10610FE13108B02F6F70FEDA178597557A8005E7, ___leftBotCam_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftBotCam_9() const { return ___leftBotCam_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftBotCam_9() { return &___leftBotCam_9; }
	inline void set_leftBotCam_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftBotCam_9 = value;
	}

	inline static int32_t get_offset_of_points_10() { return static_cast<int32_t>(offsetof(collideManagementBird_t10610FE13108B02F6F70FEDA178597557A8005E7, ___points_10)); }
	inline int32_t get_points_10() const { return ___points_10; }
	inline int32_t* get_address_of_points_10() { return &___points_10; }
	inline void set_points_10(int32_t value)
	{
		___points_10 = value;
	}

	inline static int32_t get_offset_of_scorePlayer_11() { return static_cast<int32_t>(offsetof(collideManagementBird_t10610FE13108B02F6F70FEDA178597557A8005E7, ___scorePlayer_11)); }
	inline int32_t get_scorePlayer_11() const { return ___scorePlayer_11; }
	inline int32_t* get_address_of_scorePlayer_11() { return &___scorePlayer_11; }
	inline void set_scorePlayer_11(int32_t value)
	{
		___scorePlayer_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDEMANAGEMENTBIRD_T10610FE13108B02F6F70FEDA178597557A8005E7_H
#ifndef ENDACTION_TCB18F82EC3F91864854F9923A00BBFBD8D21043E_H
#define ENDACTION_TCB18F82EC3F91864854F9923A00BBFBD8D21043E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// endAction
struct  endAction_tCB18F82EC3F91864854F9923A00BBFBD8D21043E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject endAction::bird
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___bird_4;
	// UnityEngine.Vector3 endAction::rightTopCam
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightTopCam_5;
	// UnityEngine.Vector3 endAction::rightBotCam
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightBotCam_6;
	// UnityEngine.Vector3 endAction::leftTopCam
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftTopCam_7;
	// UnityEngine.Vector3 endAction::leftBotCam
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftBotCam_8;

public:
	inline static int32_t get_offset_of_bird_4() { return static_cast<int32_t>(offsetof(endAction_tCB18F82EC3F91864854F9923A00BBFBD8D21043E, ___bird_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_bird_4() const { return ___bird_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_bird_4() { return &___bird_4; }
	inline void set_bird_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___bird_4 = value;
		Il2CppCodeGenWriteBarrier((&___bird_4), value);
	}

	inline static int32_t get_offset_of_rightTopCam_5() { return static_cast<int32_t>(offsetof(endAction_tCB18F82EC3F91864854F9923A00BBFBD8D21043E, ___rightTopCam_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightTopCam_5() const { return ___rightTopCam_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightTopCam_5() { return &___rightTopCam_5; }
	inline void set_rightTopCam_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightTopCam_5 = value;
	}

	inline static int32_t get_offset_of_rightBotCam_6() { return static_cast<int32_t>(offsetof(endAction_tCB18F82EC3F91864854F9923A00BBFBD8D21043E, ___rightBotCam_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightBotCam_6() const { return ___rightBotCam_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightBotCam_6() { return &___rightBotCam_6; }
	inline void set_rightBotCam_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightBotCam_6 = value;
	}

	inline static int32_t get_offset_of_leftTopCam_7() { return static_cast<int32_t>(offsetof(endAction_tCB18F82EC3F91864854F9923A00BBFBD8D21043E, ___leftTopCam_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftTopCam_7() const { return ___leftTopCam_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftTopCam_7() { return &___leftTopCam_7; }
	inline void set_leftTopCam_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftTopCam_7 = value;
	}

	inline static int32_t get_offset_of_leftBotCam_8() { return static_cast<int32_t>(offsetof(endAction_tCB18F82EC3F91864854F9923A00BBFBD8D21043E, ___leftBotCam_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftBotCam_8() const { return ___leftBotCam_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftBotCam_8() { return &___leftBotCam_8; }
	inline void set_leftBotCam_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftBotCam_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDACTION_TCB18F82EC3F91864854F9923A00BBFBD8D21043E_H
#ifndef GAMEOVER_T8238CCAC5153CC05D10670839980675A5543A6BA_H
#define GAMEOVER_T8238CCAC5153CC05D10670839980675A5543A6BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gameOver
struct  gameOver_t8238CCAC5153CC05D10670839980675A5543A6BA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOVER_T8238CCAC5153CC05D10670839980675A5543A6BA_H
#ifndef MOVEBK_TDF80A69114F7791D9C9C69A68D4C9A1C2A428D62_H
#define MOVEBK_TDF80A69114F7791D9C9C69A68D4C9A1C2A428D62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// moveBK
struct  moveBK_tDF80A69114F7791D9C9C69A68D4C9A1C2A428D62  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Vector2 moveBK::speed
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___speed_4;
	// System.Single moveBK::positionRestartX
	float ___positionRestartX_5;
	// UnityEngine.Vector2 moveBK::movement
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___movement_6;
	// UnityEngine.Vector3 moveBK::rightTopCam
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightTopCam_7;
	// UnityEngine.Vector3 moveBK::rightBotCam
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightBotCam_8;
	// UnityEngine.Vector3 moveBK::leftTopCam
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftTopCam_9;
	// UnityEngine.Vector3 moveBK::leftBotCam
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftBotCam_10;
	// UnityEngine.Vector3 moveBK::siz
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___siz_11;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(moveBK_tDF80A69114F7791D9C9C69A68D4C9A1C2A428D62, ___speed_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_speed_4() const { return ___speed_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_positionRestartX_5() { return static_cast<int32_t>(offsetof(moveBK_tDF80A69114F7791D9C9C69A68D4C9A1C2A428D62, ___positionRestartX_5)); }
	inline float get_positionRestartX_5() const { return ___positionRestartX_5; }
	inline float* get_address_of_positionRestartX_5() { return &___positionRestartX_5; }
	inline void set_positionRestartX_5(float value)
	{
		___positionRestartX_5 = value;
	}

	inline static int32_t get_offset_of_movement_6() { return static_cast<int32_t>(offsetof(moveBK_tDF80A69114F7791D9C9C69A68D4C9A1C2A428D62, ___movement_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_movement_6() const { return ___movement_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_movement_6() { return &___movement_6; }
	inline void set_movement_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___movement_6 = value;
	}

	inline static int32_t get_offset_of_rightTopCam_7() { return static_cast<int32_t>(offsetof(moveBK_tDF80A69114F7791D9C9C69A68D4C9A1C2A428D62, ___rightTopCam_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightTopCam_7() const { return ___rightTopCam_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightTopCam_7() { return &___rightTopCam_7; }
	inline void set_rightTopCam_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightTopCam_7 = value;
	}

	inline static int32_t get_offset_of_rightBotCam_8() { return static_cast<int32_t>(offsetof(moveBK_tDF80A69114F7791D9C9C69A68D4C9A1C2A428D62, ___rightBotCam_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightBotCam_8() const { return ___rightBotCam_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightBotCam_8() { return &___rightBotCam_8; }
	inline void set_rightBotCam_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightBotCam_8 = value;
	}

	inline static int32_t get_offset_of_leftTopCam_9() { return static_cast<int32_t>(offsetof(moveBK_tDF80A69114F7791D9C9C69A68D4C9A1C2A428D62, ___leftTopCam_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftTopCam_9() const { return ___leftTopCam_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftTopCam_9() { return &___leftTopCam_9; }
	inline void set_leftTopCam_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftTopCam_9 = value;
	}

	inline static int32_t get_offset_of_leftBotCam_10() { return static_cast<int32_t>(offsetof(moveBK_tDF80A69114F7791D9C9C69A68D4C9A1C2A428D62, ___leftBotCam_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftBotCam_10() const { return ___leftBotCam_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftBotCam_10() { return &___leftBotCam_10; }
	inline void set_leftBotCam_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftBotCam_10 = value;
	}

	inline static int32_t get_offset_of_siz_11() { return static_cast<int32_t>(offsetof(moveBK_tDF80A69114F7791D9C9C69A68D4C9A1C2A428D62, ___siz_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_siz_11() const { return ___siz_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_siz_11() { return &___siz_11; }
	inline void set_siz_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___siz_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEBK_TDF80A69114F7791D9C9C69A68D4C9A1C2A428D62_H
#ifndef MOVEPIECE_T02F2BBBFC2FFB3AE900669CBEB15EBB905F07ED1_H
#define MOVEPIECE_T02F2BBBFC2FFB3AE900669CBEB15EBB905F07ED1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// movePiece
struct  movePiece_t02F2BBBFC2FFB3AE900669CBEB15EBB905F07ED1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject movePiece::piece
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___piece_4;
	// UnityEngine.GameObject movePiece::pipeup
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___pipeup_5;

public:
	inline static int32_t get_offset_of_piece_4() { return static_cast<int32_t>(offsetof(movePiece_t02F2BBBFC2FFB3AE900669CBEB15EBB905F07ED1, ___piece_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_piece_4() const { return ___piece_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_piece_4() { return &___piece_4; }
	inline void set_piece_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___piece_4 = value;
		Il2CppCodeGenWriteBarrier((&___piece_4), value);
	}

	inline static int32_t get_offset_of_pipeup_5() { return static_cast<int32_t>(offsetof(movePiece_t02F2BBBFC2FFB3AE900669CBEB15EBB905F07ED1, ___pipeup_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_pipeup_5() const { return ___pipeup_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_pipeup_5() { return &___pipeup_5; }
	inline void set_pipeup_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___pipeup_5 = value;
		Il2CppCodeGenWriteBarrier((&___pipeup_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEPIECE_T02F2BBBFC2FFB3AE900669CBEB15EBB905F07ED1_H
#ifndef MOVEPIPE_T6D0094C8A4861DD68281F5DB69D1847251347FD7_H
#define MOVEPIPE_T6D0094C8A4861DD68281F5DB69D1847251347FD7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// movePipe
struct  movePipe_t6D0094C8A4861DD68281F5DB69D1847251347FD7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Vector2 movePipe::movement
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___movement_4;
	// UnityEngine.GameObject movePipe::pipe1Up
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___pipe1Up_5;
	// UnityEngine.GameObject movePipe::pipe1Down
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___pipe1Down_6;
	// UnityEngine.GameObject movePipe::piece
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___piece_7;
	// UnityEngine.Transform movePipe::pipe1UpOriginalTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___pipe1UpOriginalTransform_8;
	// UnityEngine.Transform movePipe::pipe1DownOriginalTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___pipe1DownOriginalTransform_9;
	// UnityEngine.Vector3 movePipe::siz
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___siz_10;
	// UnityEngine.Vector3 movePipe::rightTopCam
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightTopCam_11;
	// UnityEngine.Vector3 movePipe::rightBotCam
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightBotCam_12;
	// UnityEngine.Vector3 movePipe::leftTopCam
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftTopCam_13;
	// UnityEngine.Vector3 movePipe::leftBotCam
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftBotCam_14;
	// UnityEngine.Vector3 movePipe::up
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___up_15;
	// UnityEngine.Vector3 movePipe::down
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___down_16;

public:
	inline static int32_t get_offset_of_movement_4() { return static_cast<int32_t>(offsetof(movePipe_t6D0094C8A4861DD68281F5DB69D1847251347FD7, ___movement_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_movement_4() const { return ___movement_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_movement_4() { return &___movement_4; }
	inline void set_movement_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___movement_4 = value;
	}

	inline static int32_t get_offset_of_pipe1Up_5() { return static_cast<int32_t>(offsetof(movePipe_t6D0094C8A4861DD68281F5DB69D1847251347FD7, ___pipe1Up_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_pipe1Up_5() const { return ___pipe1Up_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_pipe1Up_5() { return &___pipe1Up_5; }
	inline void set_pipe1Up_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___pipe1Up_5 = value;
		Il2CppCodeGenWriteBarrier((&___pipe1Up_5), value);
	}

	inline static int32_t get_offset_of_pipe1Down_6() { return static_cast<int32_t>(offsetof(movePipe_t6D0094C8A4861DD68281F5DB69D1847251347FD7, ___pipe1Down_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_pipe1Down_6() const { return ___pipe1Down_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_pipe1Down_6() { return &___pipe1Down_6; }
	inline void set_pipe1Down_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___pipe1Down_6 = value;
		Il2CppCodeGenWriteBarrier((&___pipe1Down_6), value);
	}

	inline static int32_t get_offset_of_piece_7() { return static_cast<int32_t>(offsetof(movePipe_t6D0094C8A4861DD68281F5DB69D1847251347FD7, ___piece_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_piece_7() const { return ___piece_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_piece_7() { return &___piece_7; }
	inline void set_piece_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___piece_7 = value;
		Il2CppCodeGenWriteBarrier((&___piece_7), value);
	}

	inline static int32_t get_offset_of_pipe1UpOriginalTransform_8() { return static_cast<int32_t>(offsetof(movePipe_t6D0094C8A4861DD68281F5DB69D1847251347FD7, ___pipe1UpOriginalTransform_8)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_pipe1UpOriginalTransform_8() const { return ___pipe1UpOriginalTransform_8; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_pipe1UpOriginalTransform_8() { return &___pipe1UpOriginalTransform_8; }
	inline void set_pipe1UpOriginalTransform_8(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___pipe1UpOriginalTransform_8 = value;
		Il2CppCodeGenWriteBarrier((&___pipe1UpOriginalTransform_8), value);
	}

	inline static int32_t get_offset_of_pipe1DownOriginalTransform_9() { return static_cast<int32_t>(offsetof(movePipe_t6D0094C8A4861DD68281F5DB69D1847251347FD7, ___pipe1DownOriginalTransform_9)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_pipe1DownOriginalTransform_9() const { return ___pipe1DownOriginalTransform_9; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_pipe1DownOriginalTransform_9() { return &___pipe1DownOriginalTransform_9; }
	inline void set_pipe1DownOriginalTransform_9(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___pipe1DownOriginalTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___pipe1DownOriginalTransform_9), value);
	}

	inline static int32_t get_offset_of_siz_10() { return static_cast<int32_t>(offsetof(movePipe_t6D0094C8A4861DD68281F5DB69D1847251347FD7, ___siz_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_siz_10() const { return ___siz_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_siz_10() { return &___siz_10; }
	inline void set_siz_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___siz_10 = value;
	}

	inline static int32_t get_offset_of_rightTopCam_11() { return static_cast<int32_t>(offsetof(movePipe_t6D0094C8A4861DD68281F5DB69D1847251347FD7, ___rightTopCam_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightTopCam_11() const { return ___rightTopCam_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightTopCam_11() { return &___rightTopCam_11; }
	inline void set_rightTopCam_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightTopCam_11 = value;
	}

	inline static int32_t get_offset_of_rightBotCam_12() { return static_cast<int32_t>(offsetof(movePipe_t6D0094C8A4861DD68281F5DB69D1847251347FD7, ___rightBotCam_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightBotCam_12() const { return ___rightBotCam_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightBotCam_12() { return &___rightBotCam_12; }
	inline void set_rightBotCam_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightBotCam_12 = value;
	}

	inline static int32_t get_offset_of_leftTopCam_13() { return static_cast<int32_t>(offsetof(movePipe_t6D0094C8A4861DD68281F5DB69D1847251347FD7, ___leftTopCam_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftTopCam_13() const { return ___leftTopCam_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftTopCam_13() { return &___leftTopCam_13; }
	inline void set_leftTopCam_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftTopCam_13 = value;
	}

	inline static int32_t get_offset_of_leftBotCam_14() { return static_cast<int32_t>(offsetof(movePipe_t6D0094C8A4861DD68281F5DB69D1847251347FD7, ___leftBotCam_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftBotCam_14() const { return ___leftBotCam_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftBotCam_14() { return &___leftBotCam_14; }
	inline void set_leftBotCam_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftBotCam_14 = value;
	}

	inline static int32_t get_offset_of_up_15() { return static_cast<int32_t>(offsetof(movePipe_t6D0094C8A4861DD68281F5DB69D1847251347FD7, ___up_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_up_15() const { return ___up_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_up_15() { return &___up_15; }
	inline void set_up_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___up_15 = value;
	}

	inline static int32_t get_offset_of_down_16() { return static_cast<int32_t>(offsetof(movePipe_t6D0094C8A4861DD68281F5DB69D1847251347FD7, ___down_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_down_16() const { return ___down_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_down_16() { return &___down_16; }
	inline void set_down_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___down_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEPIPE_T6D0094C8A4861DD68281F5DB69D1847251347FD7_H
#ifndef TOUCHACTION_T3D74B780A5F29E015096A42B6C42736DC2274AA8_H
#define TOUCHACTION_T3D74B780A5F29E015096A42B6C42736DC2274AA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// touchAction
struct  touchAction_t3D74B780A5F29E015096A42B6C42736DC2274AA8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Vector2 touchAction::speed
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___speed_4;
	// UnityEngine.Vector2 touchAction::movement
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___movement_5;
	// UnityEngine.Vector3 touchAction::siz
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___siz_6;
	// UnityEngine.Rigidbody2D touchAction::rigid
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ___rigid_7;
	// UnityEngine.Animator touchAction::anim
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___anim_8;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(touchAction_t3D74B780A5F29E015096A42B6C42736DC2274AA8, ___speed_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_speed_4() const { return ___speed_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_movement_5() { return static_cast<int32_t>(offsetof(touchAction_t3D74B780A5F29E015096A42B6C42736DC2274AA8, ___movement_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_movement_5() const { return ___movement_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_movement_5() { return &___movement_5; }
	inline void set_movement_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___movement_5 = value;
	}

	inline static int32_t get_offset_of_siz_6() { return static_cast<int32_t>(offsetof(touchAction_t3D74B780A5F29E015096A42B6C42736DC2274AA8, ___siz_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_siz_6() const { return ___siz_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_siz_6() { return &___siz_6; }
	inline void set_siz_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___siz_6 = value;
	}

	inline static int32_t get_offset_of_rigid_7() { return static_cast<int32_t>(offsetof(touchAction_t3D74B780A5F29E015096A42B6C42736DC2274AA8, ___rigid_7)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get_rigid_7() const { return ___rigid_7; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of_rigid_7() { return &___rigid_7; }
	inline void set_rigid_7(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		___rigid_7 = value;
		Il2CppCodeGenWriteBarrier((&___rigid_7), value);
	}

	inline static int32_t get_offset_of_anim_8() { return static_cast<int32_t>(offsetof(touchAction_t3D74B780A5F29E015096A42B6C42736DC2274AA8, ___anim_8)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_anim_8() const { return ___anim_8; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_anim_8() { return &___anim_8; }
	inline void set_anim_8(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___anim_8 = value;
		Il2CppCodeGenWriteBarrier((&___anim_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHACTION_T3D74B780A5F29E015096A42B6C42736DC2274AA8_H
#ifndef WAIT2SEC_T0252EEEDAAD43734C6526C673080AAA07B3759BB_H
#define WAIT2SEC_T0252EEEDAAD43734C6526C673080AAA07B3759BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// wait2Sec
struct  wait2Sec_t0252EEEDAAD43734C6526C673080AAA07B3759BB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAIT2SEC_T0252EEEDAAD43734C6526C673080AAA07B3759BB_H
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * m_Items[1];

public:
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m41E09C4CA476451FE275573062956CED105CB79A_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m3FED1FF44F93EF1C3A07526800331B638EF4105B_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);

// UnityEngine.GameObject[] UnityEngine.GameObject::FindGameObjectsWithTag(System.String)
extern "C" IL2CPP_METHOD_ATTR GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* GameObject_FindGameObjectsWithTag_mF49A195F19A598C5FD145FFE175ABE5B4885FAD9 (String_t* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, float p0, float p1, float p2, const RuntimeMethod* method);
// UnityEngine.Object UnityEngine.Resources::Load(System.String)
extern "C" IL2CPP_METHOD_ATTR Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * Resources_Load_mDBE6B83A74A52A1A6F5A68F7E5BC112DBB81B3C1 (String_t* p0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C" IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64 (const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern "C" IL2CPP_METHOD_ATTR Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * Object_Instantiate_m1B26356F0AC5A811CA6E02F42A9E2F9C0AEA4BF0 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p1, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  p2, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * p3, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708 (RuntimeObject * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Application::LoadLevel(System.String)
extern "C" IL2CPP_METHOD_ATTR void Application_LoadLevel_m5AF197F757A77A8BEC34505328304512DE4C02B2 (String_t* p0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.GameObject::FindWithTag(System.String)
extern "C" IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * GameObject_FindWithTag_mF188C0A7A5DAC355AB020E2964B155F355237CB5 (String_t* p0, const RuntimeMethod* method);
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C" IL2CPP_METHOD_ATTR Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Camera::ViewportToWorldPoint(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Camera_ViewportToWorldPoint_m785D54092073BDEFC3B1382994E82461FD7A493F (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// System.String UnityEngine.Component::get_tag()
extern "C" IL2CPP_METHOD_ATTR String_t* Component_get_tag_mA183075586ED6BFA81D303804359AE6B02C477CC (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE (String_t* p0, String_t* p1, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<endAction>()
inline endAction_tCB18F82EC3F91864854F9923A00BBFBD8D21043E * GameObject_GetComponent_TisendAction_tCB18F82EC3F91864854F9923A00BBFBD8D21043E_m1CCB1B69336D0BC356BE5509FEEB39EE2614B834 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  endAction_tCB18F82EC3F91864854F9923A00BBFBD8D21043E * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m41E09C4CA476451FE275573062956CED105CB79A_gshared)(__this, method);
}
// System.Void endAction::stop()
extern "C" IL2CPP_METHOD_ATTR void endAction_stop_m37F7513F1A79900015EFDDC1E211A35B81F7A55B (endAction_tCB18F82EC3F91864854F9923A00BBFBD8D21043E * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<touchAction>()
inline touchAction_t3D74B780A5F29E015096A42B6C42736DC2274AA8 * GameObject_GetComponent_TistouchAction_t3D74B780A5F29E015096A42B6C42736DC2274AA8_m68E8DCDEF7883D8FEC9C0BDCE629A79B38181134 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  touchAction_t3D74B780A5F29E015096A42B6C42736DC2274AA8 * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m41E09C4CA476451FE275573062956CED105CB79A_gshared)(__this, method);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetKeyDown(System.String)
extern "C" IL2CPP_METHOD_ATTR bool Input_GetKeyDown_m0CBC4672BC86FE82A9210B7FD2F475E2DD9C3163 (String_t* p0, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
extern "C" IL2CPP_METHOD_ATTR void SceneManager_LoadScene_mFC850AC783E5EA05D6154976385DFECC251CDFB9 (String_t* p0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody2D>()
inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * Component_GetComponent_TisRigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE_mBF198078E908267FB6DA59F6242FC8F36FC06625 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3FED1FF44F93EF1C3A07526800331B638EF4105B_gshared)(__this, method);
}
// System.Void UnityEngine.Rigidbody2D::set_velocity(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_set_velocity_mE0DBCE5B683024B106C2AB6943BBA550B5BD0B83 (Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C" IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.SpriteRenderer>()
inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * GameObject_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mBAC50AEEACC94DE12B47C6D6A5227051B1C6DE12 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m41E09C4CA476451FE275573062956CED105CB79A_gshared)(__this, method);
}
// UnityEngine.Bounds UnityEngine.Renderer::get_bounds()
extern "C" IL2CPP_METHOD_ATTR Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  Renderer_get_bounds_mB29E41E26DD95939C09F3EC67F5B2793A438BDB5 (Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Bounds::get_size()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Bounds_get_size_m0739F2686AE2D3416A33AEF892653091347FD4A6 (Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * __this, float p0, float p1, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Rigidbody2D>()
inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * GameObject_GetComponent_TisRigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE_mDDB82F02C3053DCC0D60C420752A11EC11CBACC0 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m41E09C4CA476451FE275573062956CED105CB79A_gshared)(__this, method);
}
// System.Void movePipe::moveToRightPipe()
extern "C" IL2CPP_METHOD_ATTR void movePipe_moveToRightPipe_mFCC52E066E92CA573BDEACB470C000196DB7E47D (movePipe_t6D0094C8A4861DD68281F5DB69D1847251347FD7 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t Random_Range_mD0C8F37FF3CAB1D87AAA6C45130BD59626BD6780 (int32_t p0, int32_t p1, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * Component_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_m7FAA3F910786B0B5F3E0CBA755F38E0453EAF7BA (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3FED1FF44F93EF1C3A07526800331B638EF4105B_gshared)(__this, method);
}
// System.Void UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour_Invoke_m979EDEF812D4630882E2E8346776B6CA5A9176BF (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, String_t* p0, float p1, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GMPipe::Start()
extern "C" IL2CPP_METHOD_ATTR void GMPipe_Start_m9774A2D09B80C1FB622FFFC20614735EA87011D1 (GMPipe_t5F62CF8B65F621A973C7EBD778F796C789263A00 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void GMPipe::Update()
extern "C" IL2CPP_METHOD_ATTR void GMPipe_Update_mC7A2C643D177BE7ED633291A807CB3E540E51FCB (GMPipe_t5F62CF8B65F621A973C7EBD778F796C789263A00 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GMPipe_Update_mC7A2C643D177BE7ED633291A807CB3E540E51FCB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_0 = GameObject_FindGameObjectsWithTag_mF49A195F19A598C5FD145FFE175ABE5B4885FAD9(_stringLiteralAAA83EAC6890A9A6E2273EA51D6F2F2915B1A019, /*hidden argument*/NULL);
		__this->set_respawns_4(L_0);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_1 = __this->get_respawns_4();
		if ((((RuntimeArray *)L_1)->max_length))
		{
			goto IL_0087;
		}
	}
	{
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), (10.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_2 = Resources_Load_mDBE6B83A74A52A1A6F5A68F7E5BC112DBB81B3C1(_stringLiteral4DB596DEEE2F98CE13899AEF2B65181A0324BA50, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_4 = Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64(/*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_5 = __this->get_parentPipe_5();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_6 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Instantiate_m1B26356F0AC5A811CA6E02F42A9E2F9C0AEA4BF0(L_2, L_3, L_4, L_6, /*hidden argument*/NULL);
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_1), (15.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_7 = Resources_Load_mDBE6B83A74A52A1A6F5A68F7E5BC112DBB81B3C1(_stringLiteral4DB596DEEE2F98CE13899AEF2B65181A0324BA50, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = V_1;
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_9 = Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64(/*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_10 = __this->get_parentPipe_5();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_11 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_10, /*hidden argument*/NULL);
		Object_Instantiate_m1B26356F0AC5A811CA6E02F42A9E2F9C0AEA4BF0(L_7, L_8, L_9, L_11, /*hidden argument*/NULL);
	}

IL_0087:
	{
		return;
	}
}
// System.Void GMPipe::.ctor()
extern "C" IL2CPP_METHOD_ATTR void GMPipe__ctor_mE9F70960138C2002E06ECA3129D2A5A492CBD2CC (GMPipe_t5F62CF8B65F621A973C7EBD778F796C789263A00 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GameState::Start()
extern "C" IL2CPP_METHOD_ATTR void GameState_Start_m99C23A12D9F95EA8A8E1A94CE568D058F96D18D6 (GameState_t26C77D92567E12173718C059731CF9EB15949FF1 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void GameState::Update()
extern "C" IL2CPP_METHOD_ATTR void GameState_Update_m57DFDC837B7BE7E9354DB01D94CF83B568079FF9 (GameState_t26C77D92567E12173718C059731CF9EB15949FF1 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void GameState::addScore(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void GameState_addScore_mA9762E1D7BFC5B4FA7728B6EA763882B985FA7B5 (GameState_t26C77D92567E12173718C059731CF9EB15949FF1 * __this, int32_t ___toAdd0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_scorePlayer_5();
		int32_t L_1 = ___toAdd0;
		__this->set_scorePlayer_5(((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)L_1)));
		return;
	}
}
// System.Int32 GameState::getScorePlayer()
extern "C" IL2CPP_METHOD_ATTR int32_t GameState_getScorePlayer_mE2DC9BAB7A4D6508DFEA9163BD8350F607BD01F2 (GameState_t26C77D92567E12173718C059731CF9EB15949FF1 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_scorePlayer_5();
		return L_0;
	}
}
// System.Void GameState::.ctor()
extern "C" IL2CPP_METHOD_ATTR void GameState__ctor_m35AD5FDA3BD349A5EF994B454AAC3EE6421C37B2 (GameState_t26C77D92567E12173718C059731CF9EB15949FF1 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void clickButton::onClick()
extern "C" IL2CPP_METHOD_ATTR void clickButton_onClick_m27D31BDD26C0F6C7DA2171A35E9F9531FCE82855 (clickButton_tB6EF39F3026E58B94382E57B99B7AC67D7664CF1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (clickButton_onClick_m27D31BDD26C0F6C7DA2171A35E9F9531FCE82855_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(_stringLiteral79CBC5EB672A54C2B82572AFD52F2A6A4F85E598, /*hidden argument*/NULL);
		Application_LoadLevel_m5AF197F757A77A8BEC34505328304512DE4C02B2(_stringLiteralF2F001CA869A6E88BBFDF32404B845C6643460EC, /*hidden argument*/NULL);
		return;
	}
}
// System.Void clickButton::.ctor()
extern "C" IL2CPP_METHOD_ATTR void clickButton__ctor_mF7A8E93EC07D6D1BF2CFCDF37759D0509DAAF17F (clickButton_tB6EF39F3026E58B94382E57B99B7AC67D7664CF1 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void collideManagementBird::Start()
extern "C" IL2CPP_METHOD_ATTR void collideManagementBird_Start_m4B81301BAEE5A3301C7FB36D88BD380E27D962BF (collideManagementBird_t10610FE13108B02F6F70FEDA178597557A8005E7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (collideManagementBird_Start_m4B81301BAEE5A3301C7FB36D88BD380E27D962BF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = GameObject_FindWithTag_mF188C0A7A5DAC355AB020E2964B155F355237CB5(_stringLiteralCD92815BF6273ACBAF834B9FAED277C722068291, /*hidden argument*/NULL);
		__this->set_bird_4(L_0);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_1 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_2), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = Camera_ViewportToWorldPoint_m785D54092073BDEFC3B1382994E82461FD7A493F(L_1, L_2, /*hidden argument*/NULL);
		__this->set_leftBotCam_9(L_3);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_4 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_5), (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Camera_ViewportToWorldPoint_m785D54092073BDEFC3B1382994E82461FD7A493F(L_4, L_5, /*hidden argument*/NULL);
		__this->set_rightBotCam_7(L_6);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_7 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_8), (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = Camera_ViewportToWorldPoint_m785D54092073BDEFC3B1382994E82461FD7A493F(L_7, L_8, /*hidden argument*/NULL);
		__this->set_leftTopCam_8(L_9);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_10 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_11), (1.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = Camera_ViewportToWorldPoint_m785D54092073BDEFC3B1382994E82461FD7A493F(L_10, L_11, /*hidden argument*/NULL);
		__this->set_rightTopCam_6(L_12);
		__this->set_points_10(0);
		return;
	}
}
// System.Void collideManagementBird::Update()
extern "C" IL2CPP_METHOD_ATTR void collideManagementBird_Update_m3AB06E41479B03E788555E6F150F8509C8228A3B (collideManagementBird_t10610FE13108B02F6F70FEDA178597557A8005E7 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void collideManagementBird::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void collideManagementBird_OnTriggerEnter2D_m464B528284B9F16FF62ABFB611747C58BCE509CE (collideManagementBird_t10610FE13108B02F6F70FEDA178597557A8005E7 * __this, Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (collideManagementBird_OnTriggerEnter2D_m464B528284B9F16FF62ABFB611747C58BCE509CE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * L_0 = ___other0;
		String_t* L_1 = Component_get_tag_mA183075586ED6BFA81D303804359AE6B02C477CC(L_0, /*hidden argument*/NULL);
		bool L_2 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_1, _stringLiteralAAA83EAC6890A9A6E2273EA51D6F2F2915B1A019, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(_stringLiteralF86BEE40FEF30A61FB3307AB4DCB825C732FC4C1, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = __this->get_bird_4();
		endAction_tCB18F82EC3F91864854F9923A00BBFBD8D21043E * L_4 = GameObject_GetComponent_TisendAction_tCB18F82EC3F91864854F9923A00BBFBD8D21043E_m1CCB1B69336D0BC356BE5509FEEB39EE2614B834(L_3, /*hidden argument*/GameObject_GetComponent_TisendAction_tCB18F82EC3F91864854F9923A00BBFBD8D21043E_m1CCB1B69336D0BC356BE5509FEEB39EE2614B834_RuntimeMethod_var);
		endAction_stop_m37F7513F1A79900015EFDDC1E211A35B81F7A55B(L_4, /*hidden argument*/NULL);
	}

IL_002c:
	{
		Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * L_5 = ___other0;
		String_t* L_6 = Component_get_tag_mA183075586ED6BFA81D303804359AE6B02C477CC(L_5, /*hidden argument*/NULL);
		String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_6, _stringLiteral71A7CA855F38BC559D0FCE6727C7E2D8ADA99FF8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void collideManagementBird::.ctor()
extern "C" IL2CPP_METHOD_ATTR void collideManagementBird__ctor_m2484BD45AB3E356C4E368DE2893DBE06A0B8744A (collideManagementBird_t10610FE13108B02F6F70FEDA178597557A8005E7 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void endAction::Start()
extern "C" IL2CPP_METHOD_ATTR void endAction_Start_m73FAEF2E2CCCF77D662A0D562782A65B97CEEFEA (endAction_tCB18F82EC3F91864854F9923A00BBFBD8D21043E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (endAction_Start_m73FAEF2E2CCCF77D662A0D562782A65B97CEEFEA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = GameObject_FindWithTag_mF188C0A7A5DAC355AB020E2964B155F355237CB5(_stringLiteralCD92815BF6273ACBAF834B9FAED277C722068291, /*hidden argument*/NULL);
		__this->set_bird_4(L_0);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_1 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_2), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = Camera_ViewportToWorldPoint_m785D54092073BDEFC3B1382994E82461FD7A493F(L_1, L_2, /*hidden argument*/NULL);
		__this->set_leftBotCam_8(L_3);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_4 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_5), (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Camera_ViewportToWorldPoint_m785D54092073BDEFC3B1382994E82461FD7A493F(L_4, L_5, /*hidden argument*/NULL);
		__this->set_rightBotCam_6(L_6);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_7 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_8), (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = Camera_ViewportToWorldPoint_m785D54092073BDEFC3B1382994E82461FD7A493F(L_7, L_8, /*hidden argument*/NULL);
		__this->set_leftTopCam_7(L_9);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_10 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_11), (1.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = Camera_ViewportToWorldPoint_m785D54092073BDEFC3B1382994E82461FD7A493F(L_10, L_11, /*hidden argument*/NULL);
		__this->set_rightTopCam_5(L_12);
		return;
	}
}
// System.Void endAction::Update()
extern "C" IL2CPP_METHOD_ATTR void endAction_Update_m7D9297736F1B699596FE9EFA94E0558781CA55D3 (endAction_tCB18F82EC3F91864854F9923A00BBFBD8D21043E * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void endAction::stop()
extern "C" IL2CPP_METHOD_ATTR void endAction_stop_m37F7513F1A79900015EFDDC1E211A35B81F7A55B (endAction_tCB18F82EC3F91864854F9923A00BBFBD8D21043E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (endAction_stop_m37F7513F1A79900015EFDDC1E211A35B81F7A55B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_bird_4();
		touchAction_t3D74B780A5F29E015096A42B6C42736DC2274AA8 * L_1 = GameObject_GetComponent_TistouchAction_t3D74B780A5F29E015096A42B6C42736DC2274AA8_m68E8DCDEF7883D8FEC9C0BDCE629A79B38181134(L_0, /*hidden argument*/GameObject_GetComponent_TistouchAction_t3D74B780A5F29E015096A42B6C42736DC2274AA8_m68E8DCDEF7883D8FEC9C0BDCE629A79B38181134_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_1, /*hidden argument*/NULL);
		Application_LoadLevel_m5AF197F757A77A8BEC34505328304512DE4C02B2(_stringLiteral72909F5BC427175A4FF19C04C7D48487D1027642, /*hidden argument*/NULL);
		return;
	}
}
// System.Void endAction::.ctor()
extern "C" IL2CPP_METHOD_ATTR void endAction__ctor_mDD1492D98D9C38D244A931F59AB7722B71053BDA (endAction_tCB18F82EC3F91864854F9923A00BBFBD8D21043E * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void gameOver::Start()
extern "C" IL2CPP_METHOD_ATTR void gameOver_Start_mE492AED63E3E3698C07DBC6F5B56A1D78EC3FEC9 (gameOver_t8238CCAC5153CC05D10670839980675A5543A6BA * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void gameOver::Update()
extern "C" IL2CPP_METHOD_ATTR void gameOver_Update_mF4564D995D17F5286D8AD892E0F95006DD04082F (gameOver_t8238CCAC5153CC05D10670839980675A5543A6BA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (gameOver_Update_mF4564D995D17F5286D8AD892E0F95006DD04082F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Input_GetKeyDown_m0CBC4672BC86FE82A9210B7FD2F475E2DD9C3163(_stringLiteral0803DF4FF1650933D2FFE6BE04D4B21432134252, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		SceneManager_LoadScene_mFC850AC783E5EA05D6154976385DFECC251CDFB9(_stringLiteralCD8E5112CDE0E83821B265528F8F201DC85AB434, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void gameOver::.ctor()
extern "C" IL2CPP_METHOD_ATTR void gameOver__ctor_m3D2DC6020BF893AB85FA596ADEBB08D899F8C8AD (gameOver_t8238CCAC5153CC05D10670839980675A5543A6BA * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void moveBK::Start()
extern "C" IL2CPP_METHOD_ATTR void moveBK_Start_m43B365148FDFEA83FD489723E89164F03AC6E1E7 (moveBK_tDF80A69114F7791D9C9C69A68D4C9A1C2A428D62 * __this, const RuntimeMethod* method)
{
	{
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_0 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_1), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Camera_ViewportToWorldPoint_m785D54092073BDEFC3B1382994E82461FD7A493F(L_0, L_1, /*hidden argument*/NULL);
		__this->set_leftBotCam_10(L_2);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_3 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_4), (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = Camera_ViewportToWorldPoint_m785D54092073BDEFC3B1382994E82461FD7A493F(L_3, L_4, /*hidden argument*/NULL);
		__this->set_rightBotCam_8(L_5);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_6 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_7), (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = Camera_ViewportToWorldPoint_m785D54092073BDEFC3B1382994E82461FD7A493F(L_6, L_7, /*hidden argument*/NULL);
		__this->set_leftTopCam_9(L_8);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_9 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_10), (1.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = Camera_ViewportToWorldPoint_m785D54092073BDEFC3B1382994E82461FD7A493F(L_9, L_10, /*hidden argument*/NULL);
		__this->set_rightTopCam_7(L_11);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_12 = __this->get_address_of_speed_4();
		L_12->set_x_0((-1.0f));
		return;
	}
}
// System.Void moveBK::Update()
extern "C" IL2CPP_METHOD_ATTR void moveBK_Update_m91ADF12C67BC641359F2D3D06E44030B428E3D47 (moveBK_tDF80A69114F7791D9C9C69A68D4C9A1C2A428D62 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (moveBK_Update_m91ADF12C67BC641359F2D3D06E44030B428E3D47_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_0 = Component_GetComponent_TisRigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE_mBF198078E908267FB6DA59F6242FC8F36FC06625(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE_mBF198078E908267FB6DA59F6242FC8F36FC06625_RuntimeMethod_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_1 = __this->get_movement_6();
		Rigidbody2D_set_velocity_mE0DBCE5B683024B106C2AB6943BBA550B5BD0B83(L_0, L_1, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_2 = __this->get_address_of_siz_11();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * L_4 = GameObject_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mBAC50AEEACC94DE12B47C6D6A5227051B1C6DE12(L_3, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mBAC50AEEACC94DE12B47C6D6A5227051B1C6DE12_RuntimeMethod_var);
		Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  L_5 = Renderer_get_bounds_mB29E41E26DD95939C09F3EC67F5B2793A438BDB5(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Bounds_get_size_m0739F2686AE2D3416A33AEF892653091347FD4A6((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&V_0), /*hidden argument*/NULL);
		float L_7 = L_6.get_x_2();
		L_2->set_x_2(L_7);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_8 = __this->get_address_of_siz_11();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_9 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * L_10 = GameObject_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mBAC50AEEACC94DE12B47C6D6A5227051B1C6DE12(L_9, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mBAC50AEEACC94DE12B47C6D6A5227051B1C6DE12_RuntimeMethod_var);
		Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  L_11 = Renderer_get_bounds_mB29E41E26DD95939C09F3EC67F5B2793A438BDB5(L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = Bounds_get_size_m0739F2686AE2D3416A33AEF892653091347FD4A6((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&V_0), /*hidden argument*/NULL);
		float L_13 = L_12.get_y_3();
		L_8->set_y_3(L_13);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_14 = __this->get_address_of_speed_4();
		float L_15 = L_14->get_x_0();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_16 = __this->get_address_of_speed_4();
		float L_17 = L_16->get_y_1();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_18), L_15, L_17, /*hidden argument*/NULL);
		__this->set_movement_6(L_18);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_19 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_20 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_19, /*hidden argument*/NULL);
		float L_21 = L_20.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_22 = __this->get_address_of_leftBotCam_10();
		float L_23 = L_22->get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_24 = __this->get_address_of_siz_11();
		float L_25 = L_24->get_x_2();
		if ((!(((float)L_21) < ((float)((float)il2cpp_codegen_subtract((float)L_23, (float)((float)((float)L_25/(float)(2.0f)))))))))
		{
			goto IL_00e7;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_26 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		float L_27 = __this->get_positionRestartX_5();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_28 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_29 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_28, /*hidden argument*/NULL);
		float L_30 = L_29.get_y_3();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_31 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_32 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_31, /*hidden argument*/NULL);
		float L_33 = L_32.get_z_4();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_34;
		memset(&L_34, 0, sizeof(L_34));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_34), L_27, L_30, L_33, /*hidden argument*/NULL);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_26, L_34, /*hidden argument*/NULL);
	}

IL_00e7:
	{
		return;
	}
}
// System.Void moveBK::.ctor()
extern "C" IL2CPP_METHOD_ATTR void moveBK__ctor_m8D0000E6C2D76DE7DB16640FF787DA68DD39D2AD (moveBK_tDF80A69114F7791D9C9C69A68D4C9A1C2A428D62 * __this, const RuntimeMethod* method)
{
	{
		__this->set_positionRestartX_5((11.4f));
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void movePiece::Start()
extern "C" IL2CPP_METHOD_ATTR void movePiece_Start_m1ED38A10FAEC81BF681F85B420AB797A3626CAA2 (movePiece_t02F2BBBFC2FFB3AE900669CBEB15EBB905F07ED1 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void movePiece::Update()
extern "C" IL2CPP_METHOD_ATTR void movePiece_Update_mE5C164258C38A170B5F34F089F76DC7FF03F39A9 (movePiece_t02F2BBBFC2FFB3AE900669CBEB15EBB905F07ED1 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_pipeup_5();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_0, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_1, /*hidden argument*/NULL);
		float L_3 = L_2.get_x_2();
		V_0 = L_3;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = __this->get_pipeup_5();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_5 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_4, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_5, /*hidden argument*/NULL);
		float L_7 = L_6.get_y_3();
		V_1 = ((float)il2cpp_codegen_subtract((float)L_7, (float)(6.0f)));
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_8 = __this->get_piece_4();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_9 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_8, /*hidden argument*/NULL);
		float L_10 = V_0;
		float L_11 = V_1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_12), L_10, L_11, (0.0f), /*hidden argument*/NULL);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_9, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void movePiece::.ctor()
extern "C" IL2CPP_METHOD_ATTR void movePiece__ctor_m0FBEC62972025885A29AA63E4005A3C04BD391CC (movePiece_t02F2BBBFC2FFB3AE900669CBEB15EBB905F07ED1 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void movePipe::Start()
extern "C" IL2CPP_METHOD_ATTR void movePipe_Start_mAD310D623CE6B677CB968007AE9353FD3F1270B2 (movePipe_t6D0094C8A4861DD68281F5DB69D1847251347FD7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (movePipe_Start_mAD310D623CE6B677CB968007AE9353FD3F1270B2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_0 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_1), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Camera_ViewportToWorldPoint_m785D54092073BDEFC3B1382994E82461FD7A493F(L_0, L_1, /*hidden argument*/NULL);
		__this->set_leftBotCam_14(L_2);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_3 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_4), (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = Camera_ViewportToWorldPoint_m785D54092073BDEFC3B1382994E82461FD7A493F(L_3, L_4, /*hidden argument*/NULL);
		__this->set_rightBotCam_12(L_5);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_6 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_7), (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = Camera_ViewportToWorldPoint_m785D54092073BDEFC3B1382994E82461FD7A493F(L_6, L_7, /*hidden argument*/NULL);
		__this->set_leftTopCam_13(L_8);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_9 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_10), (1.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = Camera_ViewportToWorldPoint_m785D54092073BDEFC3B1382994E82461FD7A493F(L_9, L_10, /*hidden argument*/NULL);
		__this->set_rightTopCam_11(L_11);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_12 = __this->get_pipe1Up_5();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_13 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_12, /*hidden argument*/NULL);
		__this->set_pipe1UpOriginalTransform_8(L_13);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_14 = __this->get_pipe1Down_6();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_15 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_14, /*hidden argument*/NULL);
		__this->set_pipe1DownOriginalTransform_9(L_15);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(_stringLiteral2B020927D3C6EB407223A1BAA3D6CE3597A3F88D, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_16), (0.0f), (6.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_up_15(L_16);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_17), (0.0f), (-6.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_down_16(L_17);
		return;
	}
}
// System.Void movePipe::Update()
extern "C" IL2CPP_METHOD_ATTR void movePipe_Update_m0F53C305A29B3FBB513599A4B9515F8EB0DD26E8 (movePipe_t6D0094C8A4861DD68281F5DB69D1847251347FD7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (movePipe_Update_m0F53C305A29B3FBB513599A4B9515F8EB0DD26E8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_pipe1Up_5();
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_1 = GameObject_GetComponent_TisRigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE_mDDB82F02C3053DCC0D60C420752A11EC11CBACC0(L_0, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE_mDDB82F02C3053DCC0D60C420752A11EC11CBACC0_RuntimeMethod_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_2 = __this->get_movement_4();
		Rigidbody2D_set_velocity_mE0DBCE5B683024B106C2AB6943BBA550B5BD0B83(L_1, L_2, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = __this->get_pipe1Down_6();
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_4 = GameObject_GetComponent_TisRigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE_mDDB82F02C3053DCC0D60C420752A11EC11CBACC0(L_3, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE_mDDB82F02C3053DCC0D60C420752A11EC11CBACC0_RuntimeMethod_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_5 = __this->get_movement_4();
		Rigidbody2D_set_velocity_mE0DBCE5B683024B106C2AB6943BBA550B5BD0B83(L_4, L_5, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_6 = __this->get_piece_7();
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_7 = GameObject_GetComponent_TisRigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE_mDDB82F02C3053DCC0D60C420752A11EC11CBACC0(L_6, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE_mDDB82F02C3053DCC0D60C420752A11EC11CBACC0_RuntimeMethod_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_8 = __this->get_movement_4();
		Rigidbody2D_set_velocity_mE0DBCE5B683024B106C2AB6943BBA550B5BD0B83(L_7, L_8, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_9 = __this->get_address_of_siz_10();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_10 = __this->get_pipe1Up_5();
		SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * L_11 = GameObject_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mBAC50AEEACC94DE12B47C6D6A5227051B1C6DE12(L_10, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mBAC50AEEACC94DE12B47C6D6A5227051B1C6DE12_RuntimeMethod_var);
		Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  L_12 = Renderer_get_bounds_mB29E41E26DD95939C09F3EC67F5B2793A438BDB5(L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13 = Bounds_get_size_m0739F2686AE2D3416A33AEF892653091347FD4A6((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&V_0), /*hidden argument*/NULL);
		float L_14 = L_13.get_x_2();
		L_9->set_x_2(L_14);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_15 = __this->get_address_of_siz_10();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_16 = __this->get_pipe1Up_5();
		SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * L_17 = GameObject_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mBAC50AEEACC94DE12B47C6D6A5227051B1C6DE12(L_16, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mBAC50AEEACC94DE12B47C6D6A5227051B1C6DE12_RuntimeMethod_var);
		Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  L_18 = Renderer_get_bounds_mB29E41E26DD95939C09F3EC67F5B2793A438BDB5(L_17, /*hidden argument*/NULL);
		V_0 = L_18;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_19 = Bounds_get_size_m0739F2686AE2D3416A33AEF892653091347FD4A6((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&V_0), /*hidden argument*/NULL);
		float L_20 = L_19.get_y_3();
		L_15->set_y_3(L_20);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_21 = __this->get_pipe1Up_5();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_22 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_21, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_23 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_22, /*hidden argument*/NULL);
		float L_24 = L_23.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_25 = __this->get_address_of_leftBotCam_14();
		float L_26 = L_25->get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_27 = __this->get_address_of_siz_10();
		float L_28 = L_27->get_x_2();
		if ((!(((float)L_24) < ((float)((float)il2cpp_codegen_subtract((float)L_26, (float)((float)((float)L_28/(float)(2.0f)))))))))
		{
			goto IL_00cc;
		}
	}
	{
		movePipe_moveToRightPipe_mFCC52E066E92CA573BDEACB470C000196DB7E47D(__this, /*hidden argument*/NULL);
	}

IL_00cc:
	{
		return;
	}
}
// System.Void movePipe::moveToRightPipe()
extern "C" IL2CPP_METHOD_ATTR void movePipe_moveToRightPipe_mFCC52E066E92CA573BDEACB470C000196DB7E47D (movePipe_t6D0094C8A4861DD68281F5DB69D1847251347FD7 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		int32_t L_0 = Random_Range_mD0C8F37FF3CAB1D87AAA6C45130BD59626BD6780(1, 4, /*hidden argument*/NULL);
		V_0 = (((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)2)))));
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_1 = __this->get_address_of_rightBotCam_12();
		float L_2 = L_1->get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_3 = __this->get_address_of_siz_10();
		float L_4 = L_3->get_x_2();
		V_1 = ((float)il2cpp_codegen_add((float)L_2, (float)((float)((float)L_4/(float)(2.0f)))));
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_5 = __this->get_address_of_down_16();
		float L_6 = L_5->get_y_3();
		float L_7 = V_0;
		V_2 = ((float)il2cpp_codegen_add((float)L_6, (float)L_7));
		float L_8 = V_1;
		float L_9 = V_2;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_10 = __this->get_pipe1Up_5();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_11 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_10, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_11, /*hidden argument*/NULL);
		float L_13 = L_12.get_z_4();
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_3), ((float)il2cpp_codegen_add((float)L_8, (float)(3.0f))), L_9, L_13, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_14 = __this->get_pipe1Up_5();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_15 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_14, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_16 = V_3;
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_15, L_16, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_17 = __this->get_address_of_up_15();
		float L_18 = L_17->get_y_3();
		float L_19 = V_0;
		V_2 = ((float)il2cpp_codegen_add((float)L_18, (float)L_19));
		float L_20 = V_1;
		float L_21 = V_2;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_22 = __this->get_pipe1Down_6();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_23 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_22, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_24 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_23, /*hidden argument*/NULL);
		float L_25 = L_24.get_z_4();
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_3), ((float)il2cpp_codegen_add((float)L_20, (float)(3.0f))), L_21, L_25, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_26 = __this->get_pipe1Down_6();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_27 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_26, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_28 = V_3;
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_27, L_28, /*hidden argument*/NULL);
		return;
	}
}
// System.Void movePipe::.ctor()
extern "C" IL2CPP_METHOD_ATTR void movePipe__ctor_mE0742DDBABE98608BD831623E24AC16E3536A6A0 (movePipe_t6D0094C8A4861DD68281F5DB69D1847251347FD7 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void touchAction::Start()
extern "C" IL2CPP_METHOD_ATTR void touchAction_Start_mB32FE2F2862E851F7C2BAC94E0E4A9E34BE089F9 (touchAction_t3D74B780A5F29E015096A42B6C42736DC2274AA8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (touchAction_Start_mB32FE2F2862E851F7C2BAC94E0E4A9E34BE089F9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_0 = Component_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_m7FAA3F910786B0B5F3E0CBA755F38E0453EAF7BA(__this, /*hidden argument*/Component_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_m7FAA3F910786B0B5F3E0CBA755F38E0453EAF7BA_RuntimeMethod_var);
		__this->set_anim_8(L_0);
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_1 = Component_GetComponent_TisRigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE_mBF198078E908267FB6DA59F6242FC8F36FC06625(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE_mBF198078E908267FB6DA59F6242FC8F36FC06625_RuntimeMethod_var);
		__this->set_rigid_7(L_1);
		return;
	}
}
// System.Void touchAction::Update()
extern "C" IL2CPP_METHOD_ATTR void touchAction_Update_m7E0E61041918DD12F3B38A43CC7EC78969985904 (touchAction_t3D74B780A5F29E015096A42B6C42736DC2274AA8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (touchAction_Update_m7E0E61041918DD12F3B38A43CC7EC78969985904_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Input_GetKeyDown_m0CBC4672BC86FE82A9210B7FD2F475E2DD9C3163(_stringLiteral0803DF4FF1650933D2FFE6BE04D4B21432134252, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0030;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(_stringLiteralA94A8FE5CCB19BA61C4C0873D391E987982FBBD3, /*hidden argument*/NULL);
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_1 = __this->get_rigid_7();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_2), (0.0f), (5.0f), /*hidden argument*/NULL);
		Rigidbody2D_set_velocity_mE0DBCE5B683024B106C2AB6943BBA550B5BD0B83(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void touchAction::.ctor()
extern "C" IL2CPP_METHOD_ATTR void touchAction__ctor_m407CF784DA8A929D192576B8A7F2B916B2266504 (touchAction_t3D74B780A5F29E015096A42B6C42736DC2274AA8 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void wait2Sec::Start()
extern "C" IL2CPP_METHOD_ATTR void wait2Sec_Start_m28320D6AC28ACE6650B032D6011A8D6C7DBE6241 (wait2Sec_t0252EEEDAAD43734C6526C673080AAA07B3759BB * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void wait2Sec::Update()
extern "C" IL2CPP_METHOD_ATTR void wait2Sec_Update_m5762D8B269B2A5032B344805C26FAED4289374C6 (wait2Sec_t0252EEEDAAD43734C6526C673080AAA07B3759BB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (wait2Sec_Update_m5762D8B269B2A5032B344805C26FAED4289374C6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoBehaviour_Invoke_m979EDEF812D4630882E2E8346776B6CA5A9176BF(__this, _stringLiteralE17AEFFD7383EFB7F735A5AAF3599DFEFAD04B3A, (2.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void wait2Sec::goScene2()
extern "C" IL2CPP_METHOD_ATTR void wait2Sec_goScene2_m73CE9D452FC2392DAA372F155CA55AD055183EC4 (wait2Sec_t0252EEEDAAD43734C6526C673080AAA07B3759BB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (wait2Sec_goScene2_m73CE9D452FC2392DAA372F155CA55AD055183EC4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(_stringLiteralA94A8FE5CCB19BA61C4C0873D391E987982FBBD3, /*hidden argument*/NULL);
		SceneManager_LoadScene_mFC850AC783E5EA05D6154976385DFECC251CDFB9(_stringLiteralCD8E5112CDE0E83821B265528F8F201DC85AB434, /*hidden argument*/NULL);
		return;
	}
}
// System.Void wait2Sec::.ctor()
extern "C" IL2CPP_METHOD_ATTR void wait2Sec__ctor_m12A1D16D7DEA870568A7F301DA0E9DB7ABA86A2D (wait2Sec_t0252EEEDAAD43734C6526C673080AAA07B3759BB * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
