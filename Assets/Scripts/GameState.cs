﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;	

public class GameState : MonoBehaviour
{
	public static GameState Instance;
    public int scorePlayer = 0;

    public Text scoreText;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }


    public void addScore(int toAdd){
        scorePlayer+=toAdd;
    }

    public int getScorePlayer(){
        return scorePlayer;
    }
}
