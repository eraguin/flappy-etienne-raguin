﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class endAction : MonoBehaviour
{

    public GameObject bird;
            private Vector3 rightTopCam;
    private Vector3 rightBotCam;
    private Vector3 leftTopCam;
    private Vector3 leftBotCam;

    // Start is called before the first frame update
    void Start()
    {
        bird = GameObject.FindWithTag("bird");	
        leftBotCam = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
        rightBotCam = Camera.main.ViewportToWorldPoint(new Vector3(1,0,0));
        leftTopCam = Camera.main.ViewportToWorldPoint(new Vector3(0,1,0));
        rightTopCam = Camera.main.ViewportToWorldPoint(new Vector3(1,1,0));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void stop(){        
        Destroy(bird.GetComponent<touchAction>());
        Application.LoadLevel("scene4 - Game Over");

    }
}
