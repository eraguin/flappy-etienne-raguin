﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movePipe : MonoBehaviour
{

    public Vector2 movement;
    public GameObject pipe1Up;
    public GameObject pipe1Down;
    public GameObject piece;

    private Transform pipe1UpOriginalTransform;
    private Transform pipe1DownOriginalTransform;
    private Vector3 siz;

    private Vector3 rightTopCam;
    private Vector3 rightBotCam;
    private Vector3 leftTopCam;
    private Vector3 leftBotCam;

    public Vector3 up;
    public Vector3 down;

    // Start is called before the first frame update
    void Start()
    {
        leftBotCam = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
        rightBotCam = Camera.main.ViewportToWorldPoint(new Vector3(1,0,0));
        leftTopCam = Camera.main.ViewportToWorldPoint(new Vector3(0,1,0));
        rightTopCam = Camera.main.ViewportToWorldPoint(new Vector3(1,1,0));
        pipe1UpOriginalTransform = pipe1Up.transform;
        pipe1DownOriginalTransform = pipe1Down.transform;
        Debug.Log("start");
        up = new Vector3(0,6,0);
        down = new Vector3(0,-6,0);


    }

    // Update is called once per frame
    void Update()
    {
        pipe1Up.GetComponent<Rigidbody2D>().velocity = movement;
        pipe1Down.GetComponent<Rigidbody2D>().velocity = movement;
        piece.GetComponent<Rigidbody2D>().velocity = movement;

        siz.x = pipe1Up.GetComponent<SpriteRenderer>().bounds.size.x;
        siz.y = pipe1Up.GetComponent<SpriteRenderer>().bounds.size.y;
        if(pipe1Up.transform.position.x<leftBotCam.x-(siz.x/2)){
            moveToRightPipe();
        }
        
    }

    void moveToRightPipe(){
        float RandomY =Random.Range(1,4)-2;
        float posX = rightBotCam.x+(siz.x/2);
        float posY = down.y+RandomY;
        Vector3 tmpPos =new Vector3(posX+3,posY,pipe1Up.transform.position.z);
        pipe1Up.transform.position =tmpPos;
        posY = up.y+RandomY;
        tmpPos = new Vector3(posX+3,posY,pipe1Down.transform.position.z);
        pipe1Down.transform.position =tmpPos;
    }
}
