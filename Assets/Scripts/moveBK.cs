﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveBK : MonoBehaviour
{
       //vitesse de deplacement
    public Vector2 speed;
    public float positionRestartX = 11.4f;

    //stockage du mouvement
    private Vector2 movement;    
    private Vector3 rightTopCam;
    private Vector3 rightBotCam;
    private Vector3 leftTopCam;
    private Vector3 leftBotCam;
    private Vector3 siz;

    // Start is called before the first frame update
    void Start()
    {
        leftBotCam = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
        rightBotCam = Camera.main.ViewportToWorldPoint(new Vector3(1,0,0));
        leftTopCam = Camera.main.ViewportToWorldPoint(new Vector3(0,1,0));
        rightTopCam = Camera.main.ViewportToWorldPoint(new Vector3(1,1,0));
        speed.x = -1;
        
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Rigidbody2D>().velocity = movement;
        siz.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        siz.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

        //Calcul du mouvement
    movement=new Vector2(
        speed.x,
        speed.y
    );     

        if(transform.position.x < leftBotCam.x - (siz.x/2)){
            transform.position = new Vector3(positionRestartX,transform.position.y,transform.position.z);
        }
    }
}
