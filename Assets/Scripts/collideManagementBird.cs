﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collideManagementBird : MonoBehaviour
{
    public GameObject bird;
    public GameObject GameState;

    private Vector3 rightTopCam;
    private Vector3 rightBotCam;
    private Vector3 leftTopCam;
    private Vector3 leftBotCam;
    private int points;

    public int scorePlayer = 0;


    // Start is called before the first frame update
    void Start()
    {
        bird = GameObject.FindWithTag("bird");	
       // GameState = GameObject.FindWithTag("scoreLabel");
        leftBotCam = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
        rightBotCam = Camera.main.ViewportToWorldPoint(new Vector3(1,0,0));
        leftTopCam = Camera.main.ViewportToWorldPoint(new Vector3(0,1,0));
        rightTopCam = Camera.main.ViewportToWorldPoint(new Vector3(1,1,0));
        points =0;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void OnTriggerEnter2D(Collider2D other) {
        if(other.tag == "pipe"){
            Debug.Log("hit22");
            bird.GetComponent<endAction>().stop();
        }
        if(other.tag == "point"){
           // GameState.Instance.addScore(1);
        }
    }
}
